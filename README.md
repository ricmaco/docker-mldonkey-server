# [ricmaco/mldonkey-server](https://gitlab.com/ricmaco/docker-mldonkey-server)

> ❤️ this image is possible only thanks to the wonderful work made by the [LinuxServer.io](https://www.linuxserver.io/) community, please check them out!
>
> Also this README is a total rip-off of one of them, due to obvious reasons.

[MLDonkey](https://mldonkey.sourceforge.net/Main_Page) is an open-source, multi-protocol, peer-to-peer file sharing application that runs as a back-end server application on many platforms.

It can be controlled through a user interface provided by one of many separate front-ends, including a Web interface, telnet interface and over a dozen native client programs. 

This container provides the `mldonkey-server` package, which is the MLDonkey core with its own (barebones) Web interface.

[![mldonkey-logo](https://mldonkey.sourceforge.net/wiki.png)](https://mldonkey.sourceforge.net/Main_Page)

## Supported Architectures

Pulling `registry.gitlab.com/ricmaco/docker-mldonkey-server:latest` should retrieve the image for `x86-64`, but there are also other `Dockerfile`s provided.

The architectures supported are:

| Architecture | Available | Dockerfile |
| :----: | :----: | ---- |
| x86-64 | ✅ | Dockerfile |
| arm64 | ✅ | Dockerfile.aarch64 |
| armhf | ✅ | Dockerfile.armhf |

## Application Setup

Access the webui at `http://<your-ip>:4080`.

## Usage

Here are some example snippets to help you get started creating a container.

### docker-compose (recommended, [click here for more info](https://docs.linuxserver.io/general/docker-compose))

```yaml
---
version: "2.1"
services:
  mldonkey-server:
    image: registry.gitlab.com/ricmaco/docker-mldonkey-server:latest
    container_name: mldonkey-server
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Etc/UTC
    volumes:
      - /path/to/appdata/config:/config
    ports:
      - 4080:4080
    restart: unless-stopped
```

### docker cli ([click here for more info](https://docs.docker.com/engine/reference/commandline/cli/))

```bash
docker run -d \
  --name=mldonkey-server \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=Etc/UTC \
  -p 4080:4080 \
  -v /path/to/appdata/config:/config \
  --restart unless-stopped \
  registry.gitlab.com/ricmaco/docker-mldonkey-server:latest

```

## Parameters

Container images are configured using parameters passed at runtime (such as those above). These parameters are separated by a colon and indicate `<external>:<internal>` respectively. For example, `-p 8080:80` would expose port `80` from inside the container to be accessible from the host's IP on port `8080` outside the container.

| Parameter | Function |
| :----: | --- |
| `-p 4080` | web gui |
| `-e PUID=1000` | for UserID - see below for explanation |
| `-e PGID=1000` | for GroupID - see below for explanation |
| `-e TZ=Etc/UTC` | specify a timezone to use, see this [list](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones#List). |
| `-v /config` | Contains all relevant configuration files. |

## Environment variables from files (Docker secrets)

You can set any environment variable from a file by using a special prepend `FILE__`.

As an example:

```bash
-e FILE__PASSWORD=/run/secrets/mysecretpassword
```

Will set the environment variable `PASSWORD` based on the contents of the `/run/secrets/mysecretpassword` file.

## Umask for running applications

For all of our images we provide the ability to override the default umask settings for services started within the containers using the optional `-e UMASK=022` setting.
Keep in mind umask is not chmod it subtracts from permissions based on it's value it does not add. Please read up [here](https://en.wikipedia.org/wiki/Umask) before asking for support.

## User / Group Identifiers

When using volumes (`-v` flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user `PUID` and group `PGID`.

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

In this instance `PUID=1000` and `PGID=1000`, to find yours use `id user` as below:

```bash
  $ id username
    uid=1000(dockeruser) gid=1000(dockergroup) groups=1000(dockergroup)
```

## Support Info

* Shell access whilst the container is running: `docker exec -it mldonkey-server /bin/bash`
* To monitor the logs of the container in realtime: `docker logs -f mldonkey-server`
* container version number
  * `docker inspect -f '{{ index .Config.Labels "build_version" }}' mldonkey-server`
* image version number
  * `docker inspect -f '{{ index .Config.Labels "build_version" }}' registry.gitlab.com/ricmaco/docker-mldonkey-server:latest`

## Updating Info

This image is static, versioned, and requires an image update and container recreation to update the app inside. I (and [LinuxServer.io](https://www.linuxserver.io/), who originally wrote this) do not recommend or support updating apps inside the container.

Below are the instructions for updating containers:

### Via Docker Compose

* Update all images: `docker-compose pull`
  * or update a single image: `docker-compose pull mldonkey-server`
* Let compose update all containers as necessary: `docker-compose up -d`
  * or update a single container: `docker-compose up -d mldonkey-server`
* You can also remove the old dangling images: `docker image prune`

### Via Docker Run

* Update the image: `docker pull registry.gitlab.com/ricmaco/docker-mldonkey-server:latest`
* Stop the running container: `docker stop mldonkey-server`
* Delete the container: `docker rm mldonkey-server`
* Recreate a new container with the same docker run parameters as instructed above (if mapped correctly to a host folder, your `/config` folder and settings will be preserved)
* You can also remove the old dangling images: `docker image prune`

## Building locally

If you want to make local modifications to these images for development purposes or just to customize the logic:

```bash
git clone https://gitlab.com/ricmaco/docker-mldonkey-server.git
cd docker-mldonkey-server
docker build \
  --no-cache \
  --pull \
  -t registry.gitlab.com/ricmaco/docker-mldonkey-server:latest .
```

The ARM variants can be built on x86_64 hardware using `multiarch/qemu-user-static`

```bash
docker run --rm --privileged multiarch/qemu-user-static:register --reset
```

Once registered you can define the dockerfile to use with `-f Dockerfile.aarch64`.

## Versions

* **18.04.23:** - Initial Release.
